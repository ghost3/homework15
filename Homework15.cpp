#include <iostream>

void EvenOrOdd(int limitInput, bool isOdd)
{
    int i = 0;

    if (isOdd)
        ++i;

    for( ;  i < limitInput; i+=2)
        std::cout << i << '\n';
}

int main()
{
    //it's N
     const int limit = 15;

     std::cout << "All numbers: " << '\n';

    //prints some digits
    for (int i = 0; i < limit; ++i)
        std::cout << i << '\n';

    std::cout << "\nOdd numbers: " <<'\n';

    //prints odd numbers
    EvenOrOdd(limit, true);

    std::cout << "\nEven numbers: " << '\n';

    //prints even numbers
    //let's consider that 0 is even number
    EvenOrOdd(limit, false);
}


